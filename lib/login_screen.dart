import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'main_screen.dart';
import 'fire_store_helper.dart';
import 'model/user.dart';

class LoginScreen extends StatefulWidget {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = GoogleSignIn();

  @override
  LoginScreenState createState() {
    return new LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> {
  var _loading = true;

  @override
  void initState() {
    super.initState();
    moveIfLogged();
  }

  void moveIfLogged() async {
    final user = await widget._auth.currentUser();
    print("current user $user");
    if (user != null) {
      _moveToMainScreen();
    } else {
      stopLoading();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Builder(
      builder: (scaffoldContext) => Center(
          child: _loading
              ? Container(
                  width: 100.0,
                  height: 100.0,
                  child: CircularProgressIndicator(),
                )
              : RaisedButton(
                  onPressed: () {
                    _signIn(scaffoldContext);
                  },
                  child: Text(
                    "Login via Google",
                    textScaleFactor: 2.0,
                  ),
                )),
    ));
  }

  void _signIn(BuildContext scaffoldContext) async {
    setState(() {
      _loading = true;
    });
    try {
      final user = await _handleSignIn();
      print("user $user");
      if (user != null) {
        if (await _isRegistered(user, scaffoldContext)) {
          _moveToMainScreen();
        } else {
          _registerAndMove(user, scaffoldContext);
        }
      }
    } catch (error) {
      if (error is! NoSuchMethodError) {
        _showNoInternetConnection(error, scaffoldContext);
      } else {
        stopLoading();
      }
    }
  }

  Future<FirebaseUser> _handleSignIn() async {
    GoogleSignInAccount googleUser = await widget._googleSignIn.signIn();
    GoogleSignInAuthentication googleAuth = await googleUser.authentication;
    FirebaseUser user = await widget._auth.signInWithGoogle(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    print("signed in " + user.displayName);
    return user;
  }

  void _registerAndMove(final FirebaseUser user, final BuildContext scaffoldContext) async {
    if (await _register(user)) {
      _moveToMainScreen();
    } else {
      stopLoading();
      _showNoInternetConnection(null, scaffoldContext);
    }
  }

  Future<bool> _isRegistered(final FirebaseUser user, final BuildContext scaffoldContext) async {
    try {
      DocumentSnapshot userDocument = await FS().instance.collection(usersFS).document(user.uid).get();
      print("userDocument exists ${userDocument.exists}");
      return userDocument.exists;
    } catch (e) {
      _showNoInternetConnection(e, scaffoldContext);
      return false;
    }
  }

  Future<bool> _register(final FirebaseUser user) async {
    final userData = User(user.displayName);
    await FS().instance.collection(usersFS).document(user.uid).setData(userData.toJson());
    final document = await FS().instance.collection(usersFS).document(user.uid).get();
    return document.exists;
  }

  void _showNoInternetConnection(final errorToLog, final BuildContext scaffoldContext) {
    stopLoading();
    print("error $errorToLog");
    Scaffold.of(scaffoldContext).showSnackBar(SnackBar(
        backgroundColor: Theme.of(context).primaryColor,
        content: Text(
          "No internet connection",
          style: TextStyle(color: Colors.red),
          textScaleFactor: 1.5,
        )));
  }

  void _moveToMainScreen() {
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (ctx) => MainScreen()));
  }

  void stopLoading() {
    setState(() {
      _loading = false;
    });
  }
}
