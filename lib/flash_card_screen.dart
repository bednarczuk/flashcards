import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'fire_store_helper.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'model/flash_card.dart';
import 'model/flash_card_group.dart';
import 'learn_screen.dart';
import 'pair.dart';

class FlashCardScreen extends StatefulWidget {
  final FlashCardGroup _flashCardGroup;
  final String _flashGroupDocID;

  FlashCardScreen(this._flashCardGroup, this._flashGroupDocID);

  @override
  FlashCardScreenState createState() {
    return new FlashCardScreenState();
  }
}

class FlashCardScreenState extends State<FlashCardScreen> {
  FirebaseUser _user;
  int _flashCardID = -1;
  bool _cardAdding = false;
  TextEditingController _firstController = TextEditingController();
  TextEditingController _secondController = TextEditingController();
  ScrollController _listViewController = ScrollController();
  FocusNode _focusNodeFirst = FocusNode();
  FocusNode _focusNodeSecond = FocusNode();
  Map<String, bool> _backs;

  @override
  void initState() {
    super.initState();
    setupUser();
  }

  void setupUser() async {
    FirebaseAuth.instance.currentUser().then((user) {
      setState(() {
        _user = user;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
        stream: _flashCardStream(_user.uid, widget._flashGroupDocID),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          final List<Pair<String, FlashCard>> flashCards = snapshot.data == null
              ? []
              : snapshot.data.documents.map((snp) => Pair(snp.documentID, FlashCard.fromJson(snp.data))).toList();
          _backs = Map.fromIterable(flashCards,
              key: (e) => e.first,
              value: (e) {
                if (_backs != null && _backs[e.first] != null) {
                  return _backs[e.first];
                } else {
                  return false;
                }
              });

          return snapshot.data == null
              ? Scaffold(
                  appBar: _appBar(),
                  body: Center(child: Container(height: 100.0, width: 100.0, child: CircularProgressIndicator())))
              : Scaffold(
                  appBar: _appBar(),
                  bottomNavigationBar: Row(
                    children: <Widget>[
                      flashCards.length == 0
                          ? SizedBox()
                          : Expanded(
                              child: Container(
                                height: 50.0,
                                child: RaisedButton(
                                  color: Colors.green,
                                  onPressed: () {
                                    Navigator.of(context).push(MaterialPageRoute(
                                        builder: (context) => LearnScreen(widget._flashGroupDocID, flashCards)));
                                  },
                                  child: Text(
                                    "Learn All",
                                    textScaleFactor: 1.5,
                                  ),
                                ),
                              ),
                            ),
                      Expanded(
                        child: Container(
                          height: 50.0,
                          child: RaisedButton(
                            color: Colors.blue,
                            onPressed: () {
                              setState(() {
                                _addNewCard();
                              });
                            },
                            child: Text(
                              "Add new",
                              textScaleFactor: 1.5,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  body: Center(
                    child: ListView.builder(
                      controller: _listViewController,
                      itemBuilder: (context, index) => _flashCard(flashCards, index),
                      itemCount: flashCards.length + 1,
                    ),
                  ),
                );
        });
  }

  Stream<QuerySnapshot> _flashCardStream(final String uid, final String cardGroup) {
    return FS()
        .instance
        .collection(usersFS)
        .document(uid)
        .collection(flashcardsGroupFS)
        .document(cardGroup)
        .collection(flashcardsFS)
        .snapshots();
  }

  Widget _appBar() {
    return AppBar(
      title: Text(widget._flashCardGroup.name),
    );
  }

  Widget _flashCard(final List<Pair<String, FlashCard>> flashCards, final int index) {
    Pair<String, FlashCard> flashCard = index < flashCards.length ? flashCards[index] : null;
    return Card(
      margin: EdgeInsets.all(1.0),
      key: ValueKey(index < flashCards.length ? flashCard.first : null),
      child: Container(
          height: 50.0,
          child: index < flashCards.length
              ? Center(
                  child: _flashCardID == index
                      ? _editableCard(flashCard.first)
                      : Row(children: <Widget>[
                          Expanded(
                            child: Container(
                              color: flashCard.second.learnt ? Colors.greenAccent : Colors.redAccent,
                            ),
                          ),
                          Expanded(
                            flex: 3,
                            child: Container(
                              height: 50.0,
                              color: Colors.blueAccent,
                              child: Center(
                                child: Text(
                                  _backs[flashCard.first]
                                      ? "${langToCode(widget._flashCardGroup.langBack)}"
                                      : "${langToCode(widget._flashCardGroup.langFront)}",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 30,
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  _backs[flashCard.first] = !_backs[flashCard.first];
                                });
                              },
                              child: Container(
                                height: 50.0,
                                child: Center(
                                  child: Text(
                                    _backs[flashCard.first] ? "${flashCard.second.back}" : "${flashCard.second.front}",
                                    textScaleFactor: 1.5,
                                    textAlign: TextAlign.center,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 9,
                            child: Container(
                              height: 50.0,
                              child: RaisedButton(
                                  color: Colors.green,
                                  child: Text("Edit"),
                                  onPressed: () {
                                    _focusFlashCard(flashCard.second, index);
                                  }),
                            ),
                          )
                        ]))
              : _cardAdding
                  ? _editableCard(null)
                  : IconButton(
                      icon: Icon(
                        Icons.add,
                        color: Colors.greenAccent,
                        size: 48.0,
                      ),
                      onPressed: () {
                        _addNewCard();
                      })),
    );
  }

  Widget _editableCard(final String documentID) {
    _focusNodeFirst = FocusNode();
    _focusNodeSecond = FocusNode();
    return Row(children: [
      Expanded(
        flex: 2,
        child: TextField(
          decoration: InputDecoration(hintText: langToString(widget._flashCardGroup.langFront)),
          autofocus: true,
          textInputAction: TextInputAction.go,
          controller: _firstController,
          focusNode: _focusNodeFirst,
          onSubmitted: (string) {
            FocusScope.of(context).requestFocus(_focusNodeSecond);
          },
          textCapitalization: TextCapitalization.sentences,
        ),
      ),
      SizedBox(
        width: 12.0,
      ),
      Expanded(
        flex: 2,
        child: TextField(
          decoration: InputDecoration(hintText: langToString(widget._flashCardGroup.langBack)),
          controller: _secondController,
          focusNode: _focusNodeSecond,
          onSubmitted: (string) {
            _saveFlashCard(documentID);
          },
          textCapitalization: TextCapitalization.sentences,
        ),
      ),
      Expanded(
        child: IconButton(
            icon: Icon(Icons.done, color: Colors.greenAccent),
            onPressed: () {
              _saveFlashCard(documentID);
            }),
      ),
      documentID == null
          ? SizedBox()
          : Expanded(
              child: IconButton(
                  icon: Icon(Icons.delete, color: Colors.redAccent),
                  onPressed: () {
                    _deleteFlashCard(documentID);
                  }),
            ),
      Expanded(
        child: IconButton(
            icon: Icon(Icons.clear, color: Colors.blueAccent),
            onPressed: () {
              setState(() {
                _flashCardID = -1;
                _cardAdding = false;
              });
            }),
      )
    ]);
  }

  void _focusFlashCard(final FlashCard flashCard, final int index) {
    setState(() {
      _cardAdding = false;
      _flashCardID = index;
      _firstController.text = flashCard.front;
      _secondController.text = flashCard.back;
    });
    _reFocus();
  }

  void _saveFlashCard(final String documentID) {
    FlashCard newFlashCard = FlashCard("", "");
    newFlashCard.front = _firstController.text;
    newFlashCard.back = _secondController.text;
    _listViewController.jumpTo(_listViewController.position.maxScrollExtent);
    setState(() {
      _firstController.text = "";
      _secondController.text = "";
      _flashCardID = null;
      if (documentID == null) {
        _cardAdding = true;
        _reFocus();
      } else {
        _cardAdding = false;
      }
    });
    FS()
        .instance
        .collection(usersFS)
        .document(_user.uid)
        .collection(flashcardsGroupFS)
        .document(widget._flashGroupDocID)
        .collection(flashcardsFS)
        .document(documentID)
        .setData(newFlashCard.toJson());
  }

  void _deleteFlashCard(final String documentID) {
    setState(() {
      _flashCardID = null;
    });
    FS()
        .instance
        .collection(usersFS)
        .document(_user.uid)
        .collection(flashcardsGroupFS)
        .document(widget._flashGroupDocID)
        .collection(flashcardsFS)
        .document(documentID)
        .delete();
  }

  void _addNewCard() async {
    _listViewController.animateTo(_listViewController.position.maxScrollExtent,
        duration: Duration(milliseconds: 100), curve: Curves.decelerate);
    _reFocus();
    _firstController.text = "";
    _secondController.text = "";
    setState(() {
      _cardAdding = true;
      _flashCardID = -1;
    });
    _reFocus();
  }

  void _reFocus() async {
    await Future.delayed(Duration(milliseconds: 300));
    FocusScope.of(context).requestFocus(_focusNodeFirst);
  }
}
