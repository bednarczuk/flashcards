// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'flash_card_group.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FlashCardGroup _$FlashCardGroupFromJson(Map<String, dynamic> json) {
  return FlashCardGroup(
      json['name'] as String,
      _$enumDecodeNullable(_$LangsEnumMap, json['langFront']),
      _$enumDecodeNullable(_$LangsEnumMap, json['langBack']));
}

Map<String, dynamic> _$FlashCardGroupToJson(FlashCardGroup instance) =>
    <String, dynamic>{
      'name': instance.name,
      'langFront': _$LangsEnumMap[instance.langFront],
      'langBack': _$LangsEnumMap[instance.langBack]
    };

T _$enumDecode<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }
  return enumValues.entries
      .singleWhere((e) => e.value == source,
          orElse: () => throw ArgumentError(
              '`$source` is not one of the supported values: '
              '${enumValues.values.join(', ')}'))
      .key;
}

T _$enumDecodeNullable<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source);
}

const _$LangsEnumMap = <Langs, dynamic>{
  Langs.ENGLISH: 'ENGLISH',
  Langs.POLISH: 'POLISH',
  Langs.GERMAN: 'GERMAN'
};
