import 'package:flutter/material.dart';
import 'model/flash_card_group.dart';
import 'fire_store_helper.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'dart:async';

class EditGroup extends StatefulWidget {
  final FlashCardGroup _flashCardGroup;
  final String _documentID;

  EditGroup(this._flashCardGroup, this._documentID);

  @override
  _EditGroupState createState() {
    return new _EditGroupState();
  }
}

class _EditGroupState extends State<EditGroup> {
  final TextEditingController _controller = TextEditingController();
  FirebaseUser _user;
  Langs _frontValue;
  Langs _backValue;
  bool _emptyName;

  @override
  void initState() {
    super.initState();
    _controller.text = widget._flashCardGroup.name;
    _frontValue = widget._flashCardGroup.langFront;
    _backValue = widget._flashCardGroup.langBack;
    _emptyName = widget._flashCardGroup.name.isEmpty;

    initializeUser();
  }

  void initializeUser() async {
    _user = await FirebaseAuth.instance.currentUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget._documentID == null ? "New fishcard folder" : "Edit ${widget._flashCardGroup.name}"),
      ),
      body: ListView(
        children: [
          Container(
              height: 75.0,
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: DropdownButton<Langs>(
                          value: _frontValue,
                          hint: Text("Front Language"),
                          items: Langs.values.map((Langs value) {
                            return DropdownMenuItem(value: value, child: Text("${langToString(value)}"));
                          }).toList(),
                          onChanged: (s) {
                            setState(() {
                              _frontValue = s;
                            });
                          })),
                  Expanded(
                      child: DropdownButton(
                          value: _backValue,
                          hint: Text("Back Language"),
                          items: Langs.values.map((Langs value) {
                            return DropdownMenuItem(value: value, child: Text("${langToString(value)}"));
                          }).toList(),
                          onChanged: (s) {
                            setState(() {
                              _backValue = s;
                            });
                          }))
                ],
              )),
          TextField(
            decoration: InputDecoration(hintText: "Folder name"),
            style: TextStyle(fontSize: 28),
            controller: _controller,
            onChanged: (string) {
              setState(() {
                _emptyName = string.isEmpty;
              });
            },
            onSubmitted: (string) {
              _save();
            },
          ),
          SizedBox(
            height: 10.0,
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: RaisedButton(
                    color: Colors.green,
                    child: Container(
                      height: 60.0,
                      child: Center(
                          child: Text(
                        "SAVE",
                        textScaleFactor: 2.0,
                      )),
                    ),
                    onPressed: _frontValue == null || _backValue == null || _emptyName ? null : _save),
              ),
              Expanded(
                  child: widget._documentID == null
                      ? RaisedButton(
                          color: Colors.blueAccent,
                          child: Container(
                            height: 60.0,
                            child: Center(
                                child: Text(
                              "CANCEL",
                              textScaleFactor: 2.0,
                            )),
                          ),
                          onPressed: _cancel)
                      : RaisedButton(
                          color: Colors.redAccent,
                          child: Container(
                            height: 60.0,
                            child: Center(
                                child: Text(
                              "DELETE",
                              textScaleFactor: 2.0,
                            )),
                          ),
                          onPressed: _delete))
            ],
          ),
        ],
      ),
    );
  }

  void _save() {
    final newName = _controller.text;
    FlashCardGroup editedGroup = widget._flashCardGroup;
    editedGroup.name = newName;
    editedGroup.langFront = _frontValue;
    editedGroup.langBack = _backValue;
    FS()
        .instance
        .collection(usersFS)
        .document(_user.uid)
        .collection(flashcardsGroupFS)
        .document(widget._documentID)
        .setData(editedGroup.toJson());
    Navigator.of(context).pop();
  }

  void _cancel() {
    Navigator.of(context).pop();
  }

  void _delete() async {
    Navigator.of(context).pop();

    final flashcards = await FS()
        .instance
        .collection(usersFS)
        .document(_user.uid)
        .collection(flashcardsGroupFS)
        .document(widget._documentID)
        .collection(flashcardsFS)
        .getDocuments();

    flashcards.documents.forEach((snapShot) {
      FS()
          .instance
          .collection(usersFS)
          .document(_user.uid)
          .collection(flashcardsGroupFS)
          .document(widget._documentID)
          .collection(flashcardsFS)
          .document(snapShot.documentID)
          .delete();
    });

    FS()
        .instance
        .collection(usersFS)
        .document(_user.uid)
        .collection(flashcardsGroupFS)
        .document(widget._documentID)
        .delete();
  }
}
