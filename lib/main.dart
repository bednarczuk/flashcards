import 'package:flutter/material.dart';
import 'login_screen.dart';
import "fire_store_helper.dart";

Future<void> main() async {
  await FS().initialize();
  runApp(MaterialApp(
    theme: ThemeData.dark(),
    home: LoginScreen(),
  ));
}
