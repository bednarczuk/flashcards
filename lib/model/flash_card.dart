import 'package:json_annotation/json_annotation.dart';

part 'flash_card.g.dart';

@JsonSerializable()
class FlashCard {
  String front;
  String back;
  bool learnt;

  FlashCard(this.front, this.back, {this.learnt = false});

  factory FlashCard.fromJson(Map<String, dynamic> json) => _$FlashCardFromJson(json);

  Map<String, dynamic> toJson() => _$FlashCardToJson(this);
}
