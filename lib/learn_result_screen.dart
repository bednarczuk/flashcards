import 'package:flutter/material.dart';
import 'model/flash_card.dart';
import 'learn_screen.dart';
import 'pair.dart';

class LearnResultScreen extends StatefulWidget {
  final List<Pair<String, FlashCard>> all;
  final List<Pair<String, FlashCard>> unknowns;
  final String _flashCardGroupDocID;

  const LearnResultScreen(this._flashCardGroupDocID, this.all, this.unknowns, {Key key}) : super(key: key);

  @override
  _LearnResultScreenState createState() => _LearnResultScreenState();
}

class _LearnResultScreenState extends State<LearnResultScreen> {
  bool _bottomBarClickable = false;

  @override
  void initState() {
    super.initState();
    makeBottomBarClickable();
  }

  void makeBottomBarClickable() async {
    await Future.delayed(Duration(milliseconds: 500));
    setState(() {
      _bottomBarClickable = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Results"),
      ),
      body: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Text(
                  "Known: ${(widget.all.length - widget.unknowns.length)}",
                  style: TextStyle(color: Colors.greenAccent),
                  textAlign: TextAlign.center,
                  textScaleFactor: 2.0,
                ),
              ),
              Expanded(
                child: Text(
                  "Unknown: ${(widget.unknowns.length)}",
                  style: TextStyle(color: Colors.redAccent),
                  textAlign: TextAlign.center,
                  textScaleFactor: 2.0,
                ),
              ),
            ],
          ),
          SizedBox(height: 24.0),
          Expanded(
            child: ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                final Pair<String, FlashCard> card = widget.all[index];
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "${card.second.front} | ${card.second.back} ",
                    textScaleFactor: 1.5,
                    style: TextStyle(color: widget.unknowns.contains(card) ? Colors.redAccent : Colors.greenAccent),
                  ),
                );
              },
              itemCount: widget.all.length,
            ),
          ),
        ],
      ),
      bottomNavigationBar: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              height: 50.0,
              child: RaisedButton(
                onPressed: _bottomBarClickable
                    ? widget.unknowns.isEmpty
                        ? () => Navigator.of(context).pop()
                        : () {
                            {
                              _moveToLearnScreen(widget.unknowns);
                            }
                          }
                    : null,
                color: Colors.redAccent,
                child: Text(
                  widget.unknowns.isEmpty ? "Exit" : "Repeat unknowns",
                  textAlign: TextAlign.center,
                  textScaleFactor: 1.5,
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              height: 50.0,
              child: RaisedButton(
                onPressed: _bottomBarClickable
                    ? () {
                        _moveToLearnScreen(widget.all);
                      }
                    : null,
                color: Colors.blueAccent,
                child: Text(
                  "Repeat all",
                  textAlign: TextAlign.center,
                  textScaleFactor: 1.5,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _moveToLearnScreen(final List<Pair<String, FlashCard>> toLearn) {
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => LearnScreen(widget._flashCardGroupDocID, toLearn)));
  }
}
