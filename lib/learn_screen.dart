import 'package:flutter/material.dart';
import 'model/flash_card.dart';
import 'learn_result_screen.dart';
import 'fire_store_helper.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'pair.dart';

class LearnScreen extends StatefulWidget {
  final String _flashCardGroupDocID;
  final List<Pair<String, FlashCard>> _flashCards;

  const LearnScreen(this._flashCardGroupDocID, this._flashCards, {Key key}) : super(key: key);

  @override
  _LearnScreenState createState() => _LearnScreenState();
}

class _LearnScreenState extends State<LearnScreen> {
  int _currentIndex = 0;
  List<Pair<String, FlashCard>> _unKnownCards = [];
  bool _bothSides = false;
  bool _buttonsDisabled = true;
  FirebaseUser _user;

  @override
  void initState() {
    super.initState();
    _enableButtons(Duration(milliseconds: 700));
  }

  void _enableButtons(final Duration duration) async {
    await Future.delayed(duration);
    setState(() {
      _buttonsDisabled = false;
    });
  }

  void _disableButtons() {
    setState(() {
      _buttonsDisabled = true;
    });
    _enableButtons(Duration(milliseconds: 300));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              widget._flashCards[_currentIndex].second.front,
              textScaleFactor: 3.0,
            ),
            SizedBox(
              height: 40.0,
            ),
            _bothSides
                ? Text(
                    widget._flashCards[_currentIndex].second.back,
                    textScaleFactor: 3.0,
                  )
                : RaisedButton(
                    onPressed: () {
                      setState(() {
                        _bothSides = true;
                      });
                    },
                    child: Text(
                      "Answer",
                      textScaleFactor: 3.0,
                    ),
                  )
          ],
        ),
      ),
      bottomNavigationBar: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              height: 50.0,
              child: RaisedButton(
                color: Colors.blueAccent,
                onPressed: _buttonsDisabled
                    ? null
                    : () {
                        _updateLearnStatus(widget._flashCards[_currentIndex], true);
                        if (_currentIndex + 1 < widget._flashCards.length) {
                          setState(() {
                            _currentIndex++;
                            _bothSides = false;
                            _disableButtons();
                          });
                        } else {
                          Navigator.of(context).pushReplacement(MaterialPageRoute(
                              builder: (context) =>
                                  LearnResultScreen(widget._flashCardGroupDocID, widget._flashCards, _unKnownCards)));
                        }
                      },
                child: Text(
                  "Know",
                  textScaleFactor: 1.5,
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              height: 50.0,
              child: RaisedButton(
                color: Colors.redAccent,
                onPressed: _buttonsDisabled
                    ? null
                    : () {
                        _updateLearnStatus(widget._flashCards[_currentIndex], false);
                        _unKnownCards.add(widget._flashCards[_currentIndex]);
                        if (_currentIndex + 1 < widget._flashCards.length) {
                          setState(() {
                            _currentIndex++;
                            _bothSides = false;
                            _disableButtons();
                          });
                        } else {
                          Navigator.of(context).pushReplacement(MaterialPageRoute(
                              builder: (context) =>
                                  LearnResultScreen(widget._flashCardGroupDocID, widget._flashCards, _unKnownCards)));
                        }
                      },
                child: Text(
                  "Don't know",
                  textScaleFactor: 1.5,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _updateLearnStatus(final Pair<String, FlashCard> flashCard, final bool learnt) async {
    if (_user == null) {
      _user = await FirebaseAuth.instance.currentUser();
      print("_user null");
    }
    FlashCard edited = FlashCard.fromJson(flashCard.second.toJson());
    edited.learnt = learnt;
    FS()
        .instance
        .collection(usersFS)
        .document(_user.uid)
        .collection(flashcardsGroupFS)
        .document(widget._flashCardGroupDocID)
        .collection(flashcardsFS)
        .document(flashCard.first)
        .setData(edited.toJson());
  }
}
