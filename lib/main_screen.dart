import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'fire_store_helper.dart';
import 'model/flash_card.dart';
import 'model/flash_card_group.dart';
import 'edit_group.dart';
import "flash_card_screen.dart";

class MainScreen extends StatefulWidget {
  @override
  MainScreenState createState() {
    return new MainScreenState();
  }
}

class MainScreenState extends State<MainScreen> {
  FirebaseUser _user;

  @override
  void initState() {
    super.initState();
    setupUser();
  }

  void setupUser() async {
    FirebaseAuth.instance.currentUser().then((user) {
      setState(() {
        _user = user;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flashcard Folders"),
      ),
      body: Center(
        child: StreamBuilder<QuerySnapshot>(
          stream: FS().instance.collection(usersFS).document(_user.uid).collection(flashcardsGroupFS).snapshots(),
          builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            final List<FlashCardGroup> flashcardGroups = snapshot.data == null
                ? []
                : snapshot.data.documents.map((snp) {
                    return FlashCardGroup.fromJson(snp.data);
                  }).toList();
            return snapshot.data == null
                ? Center(child: Container(height: 100.0, width: 100.0, child: CircularProgressIndicator()))
                : ListView.builder(
                    itemBuilder: (context, index) {
                      return Card(
                        child: Container(
                          height: 100.0,
                          child: index < flashcardGroups.length
                              ? Row(children: <Widget>[
                                  Expanded(
                                    flex: 4,
                                    child: InkWell(
                                      child: Center(
                                          child: Text(
                                        flashcardGroups[index].name,
                                        textScaleFactor: 1.5,
                                      )),
                                      onTap: () {
                                        _openFlashCardScreen(
                                            flashcardGroups[index], snapshot.data.documents[index].documentID);
                                      },
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      height: 100.0,
                                      child: RaisedButton(
                                          color: Colors.green,
                                          child: Text("Edit", textScaleFactor: 1.25,),
                                          onPressed: () {
                                            _editFlashCardGroup(
                                                flashcardGroups[index], snapshot.data.documents[index].documentID);
                                          }),
                                    ),
                                  )
                                ])
                              : RaisedButton(
                                  child: Text(
                                    "Add new Folder",
                                    textScaleFactor: 2.0,
                                  ),
                                  onPressed: () {
                                    final fcg = FlashCardGroup("", null, null);
                                    _editFlashCardGroup(fcg, null);
                                  }),
                        ),
                      );
                    },
                    itemCount: flashcardGroups.length + 1,
                  );
          },
        ),
      ),
    );
  }

  void _editFlashCardGroup(final FlashCardGroup flashCardGroup, final String documentID) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => EditGroup(flashCardGroup, documentID)));
  }

  void _openFlashCardScreen(final FlashCardGroup flashCardGroup, final String documentID) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => FlashCardScreen(flashCardGroup, documentID)));
  }
}
