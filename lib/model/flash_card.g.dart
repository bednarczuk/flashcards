// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'flash_card.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FlashCard _$FlashCardFromJson(Map<String, dynamic> json) {
  return FlashCard(json['front'] as String, json['back'] as String,
      learnt: json['learnt'] as bool);
}

Map<String, dynamic> _$FlashCardToJson(FlashCard instance) => <String, dynamic>{
      'front': instance.front,
      'back': instance.back,
      'learnt': instance.learnt
    };
