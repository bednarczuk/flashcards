import 'package:cloud_firestore/cloud_firestore.dart';

const usersFS = "users";
const flashcardsFS = "flashcards";
const flashcardsGroupFS = "flashcardgroup";

enum Langs{
  ENGLISH, POLISH, GERMAN, JAPANESE
}

String langToString(Langs lang){
  String rS = "";
  switch (lang) {
    case Langs.ENGLISH: rS = "English"; break;
    case Langs.POLISH: rS = "Polish"; break;
    case Langs.GERMAN: rS = "German"; break;
    case Langs.JAPANESE: rS = "Japanese"; break;
  }
  return rS;
}

String langToCode(Langs lang){
  String rS = "";
  switch (lang) {
    case Langs.ENGLISH: rS = "EN"; break;
    case Langs.POLISH: rS = "PL"; break;
    case Langs.GERMAN: rS = "DE"; break;
    case Langs.JAPANESE: rS = "JA"; break;
  }
  return rS;

}

class FS {
  static final FS _fs = FS._internal();
  Firestore instance = Firestore();
  static var _initialized = false;

  FS._internal();

  initialize() async {
    if (!_initialized) {
      _initialized = true;
      await instance.settings(timestampsInSnapshotsEnabled: true);
    }
  }

  factory FS() => _fs;
}
