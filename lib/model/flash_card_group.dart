import 'package:json_annotation/json_annotation.dart';
import 'package:flashcards_flutter/fire_store_helper.dart';

part 'flash_card_group.g.dart';


@JsonSerializable()
class FlashCardGroup {
  String name;
  Langs langFront;
  Langs langBack;


  FlashCardGroup(this.name, this.langFront, this.langBack);

  factory FlashCardGroup.fromJson(Map<String, dynamic> json) => _$FlashCardGroupFromJson(json);

  Map<String, dynamic> toJson() => _$FlashCardGroupToJson(this);
}
